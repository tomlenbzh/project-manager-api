export enum UserRole {
  ADMIN = 'admin',
  USER = 'user'
}

export interface IUser {
  id?: number;
  userName?: string;
  email?: string;
  password?: string;
  lang?: string;
  role?: UserRole;
  createdAt?: Date;
  updatedAt?: Date;
}
