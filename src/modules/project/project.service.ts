import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate';
import { catchError, from, map, mergeMap, Observable, throwError } from 'rxjs';
import { Repository } from 'typeorm';
import { ProjectEntity } from './models/project.entity';
import { IProject } from './models/project.interface';
import { PROJECT_CANNOT_BE_DELETED, PROJECT_COULD_NOT_BE_CREATED } from 'src/shared/constants/errors/project.errors';

@Injectable()
export class ProjectService {
  constructor(@InjectRepository(ProjectEntity) private readonly projectRepository: Repository<ProjectEntity>) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Inserts a new user in database if it doesn't already exist.
   *
   * @param     { IProject }      project
   * @returns   { Observable<IPost> }
   */
  create(project: IProject): Observable<IProject> {
    return from(this.projectRepository.save(project, {})).pipe(
      map((createdProject: IProject) => createdProject),
      catchError((err) => throwError(() => new BadRequestException(PROJECT_COULD_NOT_BE_CREATED)))
    );
  }

  /**
   * Returns a paginated list of projects belonging to a certain user.
   *
   * @param       { IPaginationOptions }      options
   * @param       { number }                  id
   * @returns     { Observable<Pagination<IProject>> }
   */
  findAllByUserPaginate(options: IPaginationOptions, id: number): Observable<Pagination<IProject>> {
    return from(paginate<IProject>(this.projectRepository, options, { where: { user: { id } } })).pipe(
      map((posts: Pagination<IProject>) => posts)
    );
  }

  /**
   * Returns a non paginated list of projects belonging to a certain user.
   *
   * @param       { number }                  id
   * @returns     { Observable<IProject[]> }
   */
  findAllByUser(id: number): Observable<IProject[]> {
    return from(
      this.projectRepository
        .createQueryBuilder('project')
        .loadRelationCountAndMap('project.tasksTotal', 'project.tasks', 'task')
        .loadRelationCountAndMap('project.tasksDone', 'project.tasks', 'task', (qb) => qb.where('task.completed = 1'))
        .where('project.user = :id', { id })
        .getMany()
    ).pipe(
      map((projects: IProject[]) => projects),
      catchError((error: Error) => throwError(() => new BadRequestException(error)))
    );
  }

  /**
   * Returns a project based on its id.
   *
   * @param       { number }      id
   * @returns     { Observable<IProject> }
   */
  findOne(id: number): Observable<IProject> {
    return from(
      this.projectRepository
        .createQueryBuilder('project')
        .loadRelationCountAndMap('project.tasksTotal', 'project.tasks', 'task')
        .loadRelationCountAndMap('project.tasksDone', 'project.tasks', 'task', (qb) => qb.where('task.completed = 1'))
        .where('project.id = :id', { id })
        .getOne()
    ).pipe(
      map((project: IProject) => project),
      catchError((error: Error) => throwError(() => new BadRequestException(error)))
    );
  }

  /**
   * Updates a single project based on its id.
   *
   * @param       { number }      id
   * @param       { IProject }    project
   * @returns     { Observable<IProject> }
   */
  updateOne(id: number, project: IProject): Observable<IProject> {
    const { updatedAt, tasksTotal, tasksDone, ...partialProject } = project;
    return from(this.projectRepository.update(id, partialProject)).pipe(
      mergeMap(() => this.findOne(id)),
      catchError((error: Error) => throwError(() => new BadRequestException(error)))
    );
  }

  /**
   * Removes a project based on its id.
   *
   * @param       { number }      id
   * @returns     { Observable<any> }
   */
  deleteOne(id: number): Observable<any> {
    return from(this.projectRepository.delete(id)).pipe(
      catchError(() => throwError(() => new BadRequestException(PROJECT_CANNOT_BE_DELETED)))
    );
  }
}
