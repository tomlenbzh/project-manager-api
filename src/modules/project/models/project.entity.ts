import { TaskEntity } from 'src/modules/tasks/models/task.entity';
import { UserEntity } from 'src/modules/user/models/user.entity';
import { GenericEntity } from 'src/shared/entities/generic.entity';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'projects' })
export class ProjectEntity extends GenericEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500, nullable: false })
  title: string;

  @Column({ length: 1000, nullable: true })
  description: string;

  @Column({ nullable: true })
  startsOn: Date;

  @Column({ nullable: true })
  dueOn: Date;

  @Column({ nullable: false, default: false })
  completed: boolean;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.posts, { onUpdate: 'CASCADE', onDelete: 'CASCADE' })
  @JoinColumn({ name: 'userId' })
  user: UserEntity;

  @OneToMany(() => TaskEntity, (task: TaskEntity) => task.project, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE'
  })
  tasks: TaskEntity[];
}
