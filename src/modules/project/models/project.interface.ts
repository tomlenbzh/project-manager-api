import { ITask } from 'src/modules/tasks/models/task.interface';

export interface IProject {
  id?: number;
  title?: string;
  description?: string;
  createdAt?: Date;
  updatedAt?: Date;
  startsOn?: Date;
  dueOn?: Date;
  user?: { id: number };
  tasks?: ITask[];
  tasksTotal?: number;
  tasksDone?: number;
  completed?: boolean;
}
