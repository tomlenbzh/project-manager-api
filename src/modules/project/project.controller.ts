import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { catchError, map, Observable } from 'rxjs';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { IProject } from './models/project.interface';
import { ProjectService } from './project.service';

@Controller('projects')
export class ProjectController {
  constructor(private projectService: ProjectService) {}

  /**
   * Inserts a new user in database if it doesn't already exist.
   *
   * @param     { IProject }      newProject
   * @returns   { Observable<IProject> }
   */
  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  create(@Body() newProject: IProject): Observable<IProject> {
    return this.projectService.create(newProject).pipe(
      map((project: IProject) => project),
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Returns a list of projects belonging to a certain user.
   *
   * @param       { ParseIntPipe }      userId
   * @returns     { Observable<Pagination<IProject, IPaginationMeta>> }
   */
  @Get('user/:user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  findAllByUser(@Param('user') userId: number): Observable<IProject[]> {
    return this.projectService.findAllByUser(Number(userId));
  }

  /**
   * Returns a project based on its id.
   *
   * @param       { string }      id
   * @returns     { Observable<IProject> }
   */
  @Get(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  findOne(@Param('id') id: string): Observable<IProject> {
    return this.projectService.findOne(Number(id));
  }

  /**
   * Updates a single project based on its id.
   *
   * @param       { string }      id
   * @param       { IProject }    project
   * @returns     { Observable<IProject> }
   */
  @Put(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  updateOne(@Param('id') id: string, @Body() project: IProject): Observable<IProject> {
    return this.projectService.updateOne(Number(id), project).pipe(
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Removes a project based on its id.
   *
   * @param       { string }      id
   * @returns     { Observable<IProject> }
   */
  @Delete(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  delete(@Param('id') id: string): Observable<IProject> {
    return this.projectService.deleteOne(Number(id));
  }
}
