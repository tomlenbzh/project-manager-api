import { ProjectEntity } from 'src/modules/project/models/project.entity';
import { UserEntity } from 'src/modules/user/models/user.entity';
import { GenericEntity } from 'src/shared/entities/generic.entity';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'tasks' })
export class TaskEntity extends GenericEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 500, nullable: false })
  title: string;

  @Column({ nullable: false, default: false })
  completed: boolean;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.posts, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
    nullable: false
  })
  @JoinColumn({ name: 'userId' })
  user: UserEntity;

  @ManyToOne(() => ProjectEntity, (project: ProjectEntity) => project.tasks, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
    nullable: false
  })
  @JoinColumn({ name: 'projectId' })
  project: ProjectEntity;
}
