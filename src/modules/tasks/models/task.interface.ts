export interface ITask {
  id?: number;
  title?: string;
  createdAt?: Date;
  updatedAt?: Date;
  user?: { id: number };
  project?: { id: number };
}
