import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { catchError, from, map, mergeMap, Observable, throwError } from 'rxjs';
import {
  TASK_CANNOT_BE_DELETED,
  TASK_COULD_NOT_BE_CREATED,
  TASK_NOT_FOUND
} from 'src/shared/constants/errors/task.errors';
import { Repository } from 'typeorm';
import { TaskEntity } from './models/task.entity';
import { ITask } from './models/task.interface';

@Injectable()
export class TaskService {
  constructor(@InjectRepository(TaskEntity) private readonly taskRepository: Repository<TaskEntity>) {}

  /**
   * Inserts a new task entity in database if it doesn't already exist.
   *
   * @param     { ITask }      task
   * @returns   { Observable<IPost> }
   */
  create(task: ITask): Observable<ITask> {
    return from(this.taskRepository.save(task)).pipe(
      map((createdPost: ITask) => createdPost),
      catchError(() => throwError(() => new BadRequestException(TASK_COULD_NOT_BE_CREATED)))
    );
  }

  /**
   * Returns a list of tasks based on their project id.
   *
   * @param       { number }      id
   * @returns     { Observable<ITask[]> }
   */
  findAllByProject(id: number): Observable<ITask[]> {
    return from(this.taskRepository.find({ where: { project: { id } } })).pipe(
      map((tasks: ITask[]) => tasks),
      catchError(() => throwError(() => new NotFoundException(TASK_NOT_FOUND)))
    );
  }

  /**
   * Returns a task based on its id.
   *
   * @param       { number }      id
   * @returns     { Observable<IProject> }
   */
  findOne(id: number): Observable<ITask> {
    return from(this.taskRepository.findOne({ where: { id } })).pipe(
      map((task: ITask) => task),
      catchError(() => throwError(() => new NotFoundException(TASK_NOT_FOUND)))
    );
  }

  /**
   * Updates a single task based on its id.
   *
   * @param       { number }      id
   * @param       { ITask }       task
   * @returns     { Observable<ITask> }
   */
  updateOne(id: number, task: ITask): Observable<ITask> {
    const { updatedAt, ...partialTask } = task;
    return from(this.taskRepository.update(id, partialTask)).pipe(
      mergeMap(() => this.findOne(id)),
      catchError((error: Error) => throwError(() => new BadRequestException(error)))
    );
  }

  /**
   * Removes a task on its id.
   *
   * @param       { number }      id
   * @returns     { Observable<any> }
   */
  deleteOne(id: number): Observable<any> {
    return from(this.taskRepository.delete(id)).pipe(
      catchError(() => throwError(() => new BadRequestException(TASK_CANNOT_BE_DELETED)))
    );
  }
}
