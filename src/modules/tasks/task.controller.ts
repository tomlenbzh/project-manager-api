import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { catchError, map, Observable } from 'rxjs';
import { JwtAuthGuard } from '../auth/guards/jwt.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { ITask } from './models/task.interface';
import { TaskService } from './task.service';

@Controller('tasks')
export class TaskController {
  constructor(private taskService: TaskService) {}

  /**
   * Inserts a new task entity in database if it doesn't already exist.
   *
   * @param     { ITask }      newTask
   * @returns   { Observable<IProject> }
   */
  @Post()
  @UseGuards(JwtAuthGuard, RolesGuard)
  create(@Body() newTask: ITask): Observable<ITask> {
    return this.taskService.create(newTask).pipe(
      map((project: ITask) => project),
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Returns a list of tasks based on their project id.
   *
   * @param       { string }      id
   * @returns     { Observable<IProject> }
   */
  @Get('project/:project')
  @UseGuards(JwtAuthGuard, RolesGuard)
  findByProject(@Param('project') projectId: number): Observable<ITask[]> {
    return this.taskService.findAllByProject(Number(projectId));
  }

  /**
   * Returns a task based on its id.
   *
   * @param       { string }      id
   * @returns     { Observable<IProject> }
   */
  @Get(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  findOne(@Param('id') id: string): Observable<ITask> {
    return this.taskService.findOne(Number(id));
  }

  /**
   * Updates a single task based on its id.
   *
   * @param       { string }      id
   * @param       { ITask }       task
   * @returns     { Observable<ITask> }
   */
  @Put(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  updateOne(@Param('id') id: string, @Body() task: ITask): Observable<ITask> {
    return this.taskService.updateOne(Number(id), task).pipe(
      catchError((error: Error) => {
        throw error;
      })
    );
  }

  /**
   * Removes a task based on its id.
   *
   * @param       { string }      id
   * @returns     { Observable<ITask> }
   */
  @Delete(':id')
  @UseGuards(JwtAuthGuard, RolesGuard)
  delete(@Param('id') id: string): Observable<ITask> {
    return this.taskService.deleteOne(Number(id));
  }
}
